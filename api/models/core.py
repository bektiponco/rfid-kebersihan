from pydantic import BaseModel
from typing import Optional, Literal

class Building(BaseModel):
    name: Optional[str] = None
    created_at: Optional[int] = None
    updated_at: Optional[int] = None

class ScoreType(BaseModel):
    label: str
    nilai: str
    created_at: Optional[int] = None
    updated_at: Optional[int] = None

class Score(BaseModel):
    user_id: int
    building_id: int
    scoretype_id: int
    created_at: Optional[int] = None
    updated_at: Optional[int] = None