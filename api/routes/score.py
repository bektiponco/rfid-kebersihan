import datetime
from fastapi import APIRouter, Depends, File, UploadFile
from fastapi.security import OAuth2PasswordRequestForm
from ..database import mongo as db
from ..models.core import Score
from ..utils.oauth2 import oauth2_scheme
from ..utils.file import save_file_to_static
import time
from ..utils.token import verify_token
from jwt import PyJWTError
from ..models.response import TokenExpired
import starlette.status as code
import datetime, calendar
import random

router = APIRouter()

@router.get("/read")
async def read(token: str = Depends(oauth2_scheme)):
    return db.readDB(token, db.score_db)

@router.post("/insert")
async def insert(data: Score):
    dataInsert = data
    if type(dataInsert) is not dict:
        dataInsert = dataInsert.dict()

    dataInsert["_id"] = db.getNextSequence(db.score_db)
    dataInsert['created_at'] = time.time() * 1000
    dataInsert['updated_at'] = time.time() * 1000
    db.score_db.insert_one(dataInsert)

    return {'status': code.HTTP_200_OK, 'data': dataInsert}

@router.get("/delete")
async def delete(id: int, token: str = Depends(oauth2_scheme)):
    return db.deleteDB(token, db.score_db, id)

@router.post("/update")
async def update(id: int, form: Score, token: str = Depends(oauth2_scheme)):
    return db.updateDB(token, db.score_db, id, form)
