import datetime
from fastapi import APIRouter, Depends, File, UploadFile
from fastapi.security import OAuth2PasswordRequestForm
from ..database import mongo as db
from ..models.core import Building
from ..utils.oauth2 import oauth2_scheme
from ..utils.file import save_file_to_static

router = APIRouter()

@router.get("/read")
async def read(token: str = Depends(oauth2_scheme)):
    return db.readDB(token, db.building_db)

@router.post("/insert")
async def insert(form: Building, token: str = Depends(oauth2_scheme)):
    return db.insertDB(token, db.building_db, form)

@router.get("/delete")
async def delete(id: int, token: str = Depends(oauth2_scheme)):
    return db.deleteDB(token, db.building_db, id)

@router.post("/update")
async def update(id: int, form: Building, token: str = Depends(oauth2_scheme)):
    return db.updateDB(token, db.building_db, id, form)
