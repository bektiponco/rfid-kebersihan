FROM tiangolo/uvicorn-gunicorn:python3.8-alpine3.10
COPY ./ /app
WORKDIR /app
RUN pip install --no-cache-dir -r requirements.txt
RUN apk add tzdata
ENV PATH=$PATH:/app
ENV PYTHONPATH "${PYTHONPATH}:/app"
EXPOSE 8000
WORKDIR /app/api
CMD /usr/local/bin/python main.py
